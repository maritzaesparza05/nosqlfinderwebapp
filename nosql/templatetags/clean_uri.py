from django import template
from nosql.services import utils

register = template.Library()

@register.filter
def clean_uri(uri):
    uri = utils.remove_accents(uri)
    uri = utils.clean_uri(uri)
    return uri