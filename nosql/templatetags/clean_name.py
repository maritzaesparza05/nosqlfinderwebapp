from django import template

register = template.Library()

@register.filter
def clean_name(name):
    return name.replace('.', '')