from django import template

register = template.Library()

@register.simple_tag
def get_amchart_index(product_name, comparision_array):
	nextIndex = 0;
	for i, product in enumerate(comparision_array):
		if product_name == product['name']:
			nextIndex = i-1
	if nextIndex < len(comparision_array):
		return nextIndex
	return 0