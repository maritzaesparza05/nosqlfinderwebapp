from django import template

register = template.Library()

@register.filter
def has_cross_platform(ops):
    ops = [op.name for op in ops if op]
    return 'cross-platform' in ops