from .models import StackoverflowTag, Repository, AlternativetoPost, StacksharePost
from .models import DBEnginePost, WikipediaPost, Developer, Repository, NoSQL, OperatingSystem

from py2neo import Graph, Node, Relationship
from django.conf import settings

from .models import ProgrammingLanguage
from neomodel import db

from nosql.services import utils
from nosqlEngine import settings
import os


class HomepageFilter:

    def __init__(self):
        self.graph = Graph('bolt://<<NEO4J_IP_ADDRESS>>:7687', user='<<NEO4J_USER>>', password='<<NEO4J_PASSWORD>>')

    def filter_by_similar_languages(self, movements, datamodels, languages):
        influenced_languages = self._filter_influenced_by_languages(languages)
        languages_uris = self._filter_languages(influenced_languages)
        result = self.filter(movements, datamodels, languages_uris)
        return result

    def filter_by_paradigm_languages(self, movements, datamodels, languages):
        paradigm_languages = self._filter_by_paradigm_languages(languages)
        languages_uris = self._filter_languages(paradigm_languages)
        result = self.filter(movements, datamodels, languages_uris)
        return result
        
    def filter(self, movements, datamodels, languages):
        datamodels_uris = self._filter_datamodels(datamodels)
        languages_uris = self._filter_languages(languages)
        movements_uris = self._filter_movements(movements)
        list_of_sets = [set(datamodels_uris), set(languages_uris), set(movements_uris)]
        non_empties = [x for x in list_of_sets if x]
        result_uris = list(set.intersection(*non_empties))
        result = self._inflate_values(result_uris)
        return list(result)

    def filter_by_datamodels_and_licenses(self, movements, datamodels):
        datamodels_uris = self._filter_datamodels(datamodels)
        movements_uris = self._filter_movements(movements)
        result_uris = self._intersect_pair(datamodels_uris, movements_uris)
        result = self._inflate_values(result_uris)
        return list(result)

    def filter_by_best_performance(self):
        query = """MATCH (n:NoSQL)-[:HAS]-(r:Review) 
        RETURN n ORDER BY r.avg_sentiment DESC LIMIT 5"""
        cursor = self.graph.run(query)
        uris = [n['n']['uri'] for n in cursor]
        result = self._inflate_values(uris)
        return list(result)

    def _filter_by_paradigm_languages(self, languages):
        query = """MATCH (p:ProgrammingLanguage)-[:SUPPORTS]-(:ProgrammingParadigm)-[:SUPPORTS]-(op:ProgrammingLanguage)
        WHERE p.dbpedia_uri IN ['Clojure'] RETURN op.dbpedia_uri""".format(languages)
        cursor = self.graph.run(query)
        uris = [p['p.dbpedia_uri']['dbpedia_uri'] for p in cursor]
        return uris 

    def _filter_influenced_by_languages(self, languages):
        query = """MATCH (p:ProgrammingLanguage)-[r:INFLUENCED_BY]-(ip:ProgrammingLanguage)
        WHERE ip.dbpedia_uri IN {} RETURN p.dbpedia_uri""".format(languages)
        cursor = self.graph.run(query)
        uris = [p['p.dbpedia_uri'] for p in cursor]
        return uris

    def _intersect(self, a, b, c):
        return list(set(a) & set(b) & set(c))

    def _intersect_pair(self, a, b):
        return list(set(a) & set(b))

    def _filter_datamodels(self, datamodels):
        query = """MATCH (n:NoSQL)-[:TYPE]->(d:DataModel)
        WHERE d.dbpedia_uri in {} RETURN n""".format(datamodels)
        cursor = self.graph.run(query)
        uris = [d['n']['uri'] for d in cursor]
        return uris

    def _filter_movements(self, movements):
        query = """MATCH (n:NoSQL)-[*1..2]->(m:Movement)
        WHERE m.dbpedia_uri IN {0} RETURN n
        UNION
        MATCH (n:NoSQL)-[r:SAME_AS]->(same)-[:APPROVED_BY*1..2]->(m:Movement)
        WHERE m.dbpedia_uri IN {0} RETURN n""".format(movements)
        cursor = self.graph.run(query)
        uris = [n['n']['uri'] for n in cursor]
        return uris

    def _filter_languages(self, languages):
        query = """MATCH (n:NoSQL)-[:SUPPORTS]-(p:ProgrammingLanguage)
        WHERE p.dbpedia_uri IN {0} RETURN n
        UNION
        MATCH (n:NoSQL)-[:SAME_AS]-(same:StackoverflowTag)-[r:SUPPORTS]-(p:ProgrammingLanguage)
        WHERE p.dbpedia_uri IN {0} RETURN n
        UNION
        MATCH (n:NoSQL)-[:HAS]-(dr:DriverReposCluster)-[:FOR]-(p:ProgrammingLanguage)
        WHERE dr.count > 0 AND p.dbpedia_uri IN {0} RETURN n""".format(languages)
        cursor = self.graph.run(query)
        uris = [n['n']['uri'] for n in cursor]
        return uris

    def _inflate_values(self, uris):
        query = """MATCH (n:NoSQL)
        WHERE n.uri IN {}
        OPTIONAL MATCH (n)-[:HAS]-(re:Review)
        OPTIONAL MATCH (n)-[:HAS]-(k:Keyword)
        OPTIONAL MATCH (n)-[:TYPE]-(d:DataModel)
        OPTIONAL MATCH (n)-[:SAME_AS]-(same) WITH n, COUNT(DISTINCT same) AS counter,
        COLLECT(same.summary) AS summaries, re, k, d
        RETURN DISTINCT n.uri AS uri, n.name AS name, n.thumbnail AS thumbnail, n.summary AS summary, COLLECT(DISTINCT k.name) as keywords, COLLECT(DISTINCT d.name) as datamodels, 
        FILTER(i in summaries WHERE size(i) > 1) AS other_summaries,
        re.total_rating as total_rating, counter ORDER BY total_rating DESC, counter DESC""".format(uris)
        cursor = self.graph.run(query)
        return cursor





graph = Graph('bolt://<<NEO4J_IP_ADDRESS>>:7687', user='<<NEO4J_USER>>', password='<<NEO4J_PASSWORD>>')


def is_database_empty():
    query = """MATCH (n) RETURN COUNT(n)"""
    node_count = graph.run(query).evaluate()
    return node_count == 0

def get_max_nosql_count_sames():
    query = """MATCH (n:NoSQL)-[r:SAME_AS]-(same) WITH COUNT(r) AS samesCount,n  
    RETURN samesCount ORDER BY samesCount DESC LIMIT 1"""
    count = graph.run(query).evaluate()
    return count

def get_count_sames(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[r:SAME_AS]-(sames) 
    RETURN COUNT(r)""".format(uri)
    count = graph.run(query).evaluate()
    return count

def get_datamodel_count():
    query = """MATCH (d:DataModel)-[r:TYPE]-(n:NoSQL) 
    RETURN d.dbpedia_uri, d.name, COUNT(r)"""
    results, meta = db.cypher_query(query)
    results = [{'dbpedia_uri': r[0], 'name': r[1], 'count': r[2]} for r in results]
    return results


def get_popularity(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}}) WITH n 
    OPTIONAL MATCH (n)-[:SAME_AS]-(st:StackoverflowTag)
    OPTIONAL MATCH (n)-[:HAS]-(re:Repository)
    RETURN st.number_questions, st.unanswered_questions, re.stars, 
    CASE 
    WHEN st.number_questions > 0
    THEN (100 * st.unanswered_questions)/st.number_questions
    ELSE 0 END as percentage_unanswered""".format(uri)
    results, meta = db.cypher_query(query)
    if results:
        number_questions = results[0][0]
        unanswered_questions = results[0][1]
        github_stars = results[0][2]
        percentage_unanswered = results[0][3]

        if not number_questions:
            number_questions = 0
        if not unanswered_questions:
            unanswered_questions = 0
        if not percentage_unanswered:
            unanswered_questions = 0
        if not github_stars:
            github_stars = 0
        popularity = {'number_questions': number_questions, 
        'unanswered_questions': unanswered_questions,
        'github_stars': github_stars, 
        'percentage_unanswered': percentage_unanswered}
    else:
        popularity = {}
    return popularity


def get_performance_rating(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(rev:Review) 
    RETURN rev.total_rating""".format(uri)
    result = graph.run(query).evaluate()
    if not result:
        result = 0
    result = round(result, 1)
    return result

def get_stackoverflow_tag(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(st:StackoverflowTag) 
    RETURN st""".format(uri)
    results, meta = db.cypher_query(query)
    results = [StackoverflowTag.inflate(row[0]) for row in results]
    stackoverflow_tag = utils.get_first_or_none(results)
    return stackoverflow_tag


def get_repository(uri):
    query = """MATCH (n:NoSQL)-[:HAS]-(re:Repository)
    WHERE n.uri = '{uri}' RETURN re""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [Repository.inflate(row[0]) for row in results]
    repository = utils.get_first_or_none(results)
    return repository


def get_summary(uri):
    query = """MATCH (n:NOSQL {{uri:'{0}'}}) RETURN n.summary as summary
    UNION
    MATCH (n:NoSQL {{uri:'{0}'}})-[:SAME_AS]-(same) RETURN same.summary as summary""".format(uri)
    cursor = graph.run(query)
    for record in cursor:
        if record['summary']:
            return record['summary']
    return ""


def get_alternativeto_post(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(al:AlternativetoPost)
    WHERE n.uri = '{uri}' RETURN al""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [AlternativetoPost.inflate(row[0]) for row in results]
    alternativeto_post = utils.get_first_or_none(results)
    return alternativeto_post

def get_stackshare_comments(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:SAME_AS]-(st:StacksharePost)-[:HAS]-(c:Comment) 
    RETURN c.likes, c.text""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'likes': r[0], 'text': r[1]} for r in results]
    return results

def get_stackshare_post(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(st:StacksharePost)
    WHERE n.uri = '{uri}' RETURN st""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [StacksharePost.inflate(row[0]) for row in results]
    stackshare_post = utils.get_first_or_none(results)
    return stackshare_post


def get_dbengine_post(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(db:DBEnginePost)
    WHERE n.uri = '{uri}' RETURN db""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [DBEnginePost.inflate(row[0]) for row in results]
    dbengine_post = utils.get_first_or_none(results)
    return dbengine_post


def get_wikipedia_post(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(db:DBEnginePost)
    WHERE n.uri = '{uri}' RETURN db""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [WikipediaPost.inflate(row[0]) for row in results]
    wikipedia_post = utils.get_first_or_none(results)
    return wikipedia_post


def get_developers(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same)-[:CREATED_BY]-(d:Developer) 
    RETURN d.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:CREATED_BY]-(d:Developer) 
    RETURN d.name""".format(uri)
    results, meta = db.cypher_query(query)
    developers = [row[0] for row in results if row[0]]
    return developers


def get_datamodels(uri):
    query = """MATCH (n:NoSQL)-[:TYPE]-(d:DataModel)
    WHERE n.uri = '{uri}' RETURN d.name""".format(uri=uri)
    results, meta = db.cypher_query(query)
    results = [{'name': r[0]} for r in results]
    return results


def get_developed_languages(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:WRITTEN_IN]-(p:ProgrammingLanguage)
    RETURN p.name as lang
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(other)-[:WRITTEN_IN]-(p1:ProgrammingLanguage)
    RETURN p1.name as lang""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': row[0]} for row in results]
    return results


def get_operating_systems(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:SUPPORTS]-(op:OperatingSystem)
    RETURN op
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(op:OperatingSystem)
    RETURN op""".format(uri)
    results, meta = db.cypher_query(query)
    operating_systems = [OperatingSystem.inflate(row[0]) for row in results]
    return operating_systems


def get_supported_datatypes(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[r:SUPPORTS]-(d:DataType) 
    RETURN d.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(d:DataType)
    RETURN d.name""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': row[0]} for row in results]
    return results


def get_homepage(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}}) 
    RETURN n.homepage as homepage
    UNION
    MATCH (n:NoSQL {{uri:'{0}'}})-[:SAME_AS]-(same) 
    RETURN same.homepage as homepage""".format(uri)
    results, meta = db.cypher_query(query)
    results = [r[0] for r in results if r]
    for r in results:
        if r:
            return r
    return ''


def get_latest_release(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same) 
    RETURN same.latest_release""".format(uri)
    results, meta = db.cypher_query(query)
    results = [r[0] for r in results if r]
    for r in results:
        if r:
            return r
    return ''


def get_status(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same) 
    RETURN same.status""".format(uri)
    results, meta = db.cypher_query(query)
    results = [r[0] for r in results if r]
    for r in results:
        if r:
            return r
    return ''


def get_frequently_updated(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same) 
    RETURN same.frequently_updated""".format(uri)
    results, meta = db.cypher_query(query)
    results = [r[0] for r in results if r]
    for r in results:
        if r:
            return r
    return ''


def get_documentation(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same) 
    RETURN same.documentation""".format(uri)
    results, meta = db.cypher_query(query)
    results = [r[0] for r in results if r]
    for r in results:
        if r:
            return r
    return ''


def get_uses(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:USED_FOR]-(u:Use)
    RETURN u.name""".format(uri)
    results, meta = db.cypher_query(query)
    uses = [r[0] for r in results]
    return uses


def get_apis(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(a:Api) 
    RETURN a.name""".format(uri)
    results, meta = db.cypher_query(query)
    apis = [r[0] for r in results]
    return apis


def get_transaction_methods(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(t:TransactionMethod)
    RETURN t.name""".format(uri)
    results, meta = db.cypher_query(query)
    transactions = [r[0] for r in results]
    return transactions


def get_consistency_methods(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(c:ConsistencyMethod)
    RETURN c.name""".format(uri)
    results, meta = db.cypher_query(query)
    methods = [r[0] for r in results]
    return methods


def get_repositories_by_language(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(dc:DriverReposCluster)-[:FOR]-(p:ProgrammingLanguage)
    WHERE dc.count > 0
    RETURN p.name as language, dc.count""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'total': r[1]} for r in results]
    return results


def get_supported_technologies(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(:ClusterTechsRepositories)-[r:HAS]-(tech) 
    RETURN tech.name, r.total""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'total': r[1]} for r in results]
    return results


def get_supported_tools(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:SUPPORTS]-(t:Tool)
    RETURN t.uri, t.name, t.thumbnail""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'uri': r[0], 'name': r[1], 'thumbnail': r[2]} for r in results]
    return results


def get_drivers(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:SUPPORTS]-(p:ProgrammingLanguage) 
    RETURN p.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same:StackoverflowTag)-[:SUPPORTS]-(p:ProgrammingLanguage)
    RETURN p.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:HAS]-(dc:ClusterTechsRepositories)-[r:HAS]-(p:ProgrammingLanguage)
    WHERE r.total > 0 RETURN p.name""".format(uri)
    languages, meta = db.cypher_query(query)
    results = []
    for lang in languages:
        lang_data = {}
        lang_data['name'] = lang[0]
        lang_data['status'] = get_status_of_language(uri, lang_data['name'])
        results.append(lang_data)
    return results


def get_status_of_language(uri, lang_name):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same)-[r:SUPPORTS]-(p:ProgrammingLanguage {{name: '{1}'}}) 
    RETURN r.status""".format(uri, lang_name)
    results, meta = db.cypher_query(query)
    if results and results[0]:
        status = results[0][0]
    else:
        status = 'unknown'
    return status


def get_drivers_repositories(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(cl:DriverReposCluster)-[:HAS]-(re:Repository) 
    WITH cl, re
    MATCH (cl)-[:FOR]-(p:ProgrammingLanguage)
    RETURN p.name, p.thumbnail, p.summary, collect(re)""".format(uri)
    results, meta = db.cypher_query(query)
    drivers = []
    for i, r in enumerate(results):
        lang = {}
        lang['id'] = i
        lang['name'] = r[0]
        lang['thumbnail'] = r[1]
        lang['summary'] = r[2]
        lang['repos'] = [Repository.inflate(node) for node in r[3]]
        drivers.append(lang)
    return drivers


def get_clients(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(same)-[:USED_BY]-(c:Client)
    RETURN c.name, c.thumbnail""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'thumbnail': r[1]}
               for r in results]
    return results


def get_links(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(l:Link) 
    RETURN l.url, l.score ORDER BY l.score DESC""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'url': r[0], 'score': r[1]} for r in results]
    return results


def get_associated_tags(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(:StackoverflowTag)-[r:RELATED_TO]-(st:StackoverflowTag)
    RETURN st.name,r.weight
    ORDER BY r.weight DESC LIMIT 10""".format(uri)
    results, meta = db.cypher_query(query)
    results_percents = []
    if results:
        max_count = max([r[1] for r in results])
        results_percents = [{'name': r[0], 'count': r[1],
                             'percent': int((r[1] * 100) / max_count) if max_count > 0 else 0}
                            for r in results]
    return results_percents


def get_alternativeto_recommendations(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(:AlternativetoPost)-[:RELATED_TO]-(al:AlternativetoPost)
    WHERE n.uri = '{uri}'
    RETURN DISTINCT al.name, al.thumbnail, al.likes ORDER BY al.likes DESC""".format(uri=uri)
    results, meta = db.cypher_query(query)
    recommendations = [{'name': r[0], 'thumbnail': r[1], 'likes': r[2]}
                       for r in results]
    return recommendations


def get_interest_over_time(uri):
    query = """MATCH (n:NoSQL)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
    WHERE n.uri = '{uri}'
    RETURN y.year, y.num_queries ORDER BY y.year""".format(uri=uri)
    results, meta = db.cypher_query(query)
    interest = [{'year': r[0], 'queries': r[1]} for r in results]
    return interest


def get_shared_tags(uri):
    query = """MATCH (n:NoSQL {{ uri: '{}' }})-[:SAME_AS]-(:StackoverflowTag)-[r:RELATED_TO]-(tag:StackoverflowTag)-[:SAME_AS]-(they:NoSQL)
    WHERE n <> they AND r.weight > 0.01
    RETURN DISTINCT they.name, tag.views, r.weight ORDER BY r.weight DESC""".format(uri)
    results, meta = db.cypher_query(query)
    tags = []
    for r in results:
        data = {}
        data['name'] = r[0]
        data['views'] = r[1]
        data['weight'] = r[2]
        tags.append(data)
    if tags:
        max_count = max([t['weight'] for t in tags])
        for t in tags:
            percent = 0
            if max_count > 0:
                percent = (t['weight'] * 100) / max_count
            t['percent'] = percent
        return tags
    return []


def get_comments(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(re:Review)-[:HAS]-(c:Comment) 
    RETURN c.url, c.text, c.stars, c.sentiment, c.created_at, c.points""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'url': r[0],
                'text': r[1],
                'stars': r[2],
                'sentiment': r[3],
                'created_at': r[4],
                'points': r[5]}
               for r in results]
    positives = []
    negatives = []
    neutrals = []
    for r in results:
        if r['sentiment'] > 0.1:
            positives.append(r)
        elif r['sentiment'] < 0.1 and r['sentiment'] > 0.02:
            neutrals.append(r)
        else:
            negatives.append(r)
    comments = {'positives': positives,
                'negatives': negatives,
                'neutrals': neutrals}
    return comments


def get_comments_points(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(re:Review)-[:HAS]-(c:Comment) 
    RETURN c.url, c.text, c.stars, c.sentiment, c.created_at, c.points""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'url': r[0],
                'text': r[1],
                'stars': r[2],
                'sentiment': r[3],
                'created_at': r[4],
                'points': r[5]}
               for r in results]
    return results


def get_sentiments_by_datamodel():
    query = """MATCH (d:DataModel)-[:TYPE]-(:NoSQL)-[:HAS]-(re:Review)-[:HAS]-(c:Comment) 
    WITH COLLECT(c.sentiment) as comments, d as dm RETURN dm.name, 
    SIZE(filter(c IN comments WHERE c < 0.1)) as negatives, 
    SIZE(filter(c IN comments WHERE c > 0.1)) as positives,  SIZE(comments) as count"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'negatives': r[1], 
    'positives': r[2], 'count': r[3]}
                for r in results]
    return results


def get_comments_text(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(re:Review)-[:HAS]-(c:Comment) 
    RETURN c.url, c.text, c.stars, c.sentiment""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'url': r[0], 'text': r[1], 'stars': r[2], 'sentiment': r[3],
                'percents': utils.get_sentiment_percent_range(r[3]),
                'category': utils.get_sentiment_category(r[3])}
               for r in results]
    return results


def get_percents_comments(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(re:Review)-[:HAS]-(c:Comment)
    RETURN c.sentiment""".format(uri)
    results, meta = db.cypher_query(query)
    total = len(results)
    positives = 0
    negatives = 0
    neutrals = 0
    for r in results:
        if r[0] > 0.1:
            positives += 1
        elif r[0] < 0.1 and r[0] > 0.02:
            neutrals += 1
        else:
            negatives += 1
    if total > 0:
        positive_percent = (positives * 100) / total
        negative_percent = (negatives * 100) / total
        neutral_percent = (neutrals * 100) / total
    else:
        positive_percent = 0
        negative_percent = 0
        neutral_percent = 0
    data = {'positives_count': positives,
            'negatives_count': negatives,
            'neutral_count': neutrals,
            'positive_percent': positive_percent,
            'negative_percent': negative_percent,
            'neutral_percent': neutral_percent}
    return data

def get_percent_unanswered_questions(uri):
    query = """MATCH (n:NoSQL {{ uri: '{}'}})-[:SAME_AS]-(st:StackoverflowTag)
    WHERE st.number_questions > 0
    RETURN (100 * st.unanswered_questions) / st.number_questions""".format(uri)
    result = graph.run(query).evaluate()
    default_percent = 0
    if result:
        return result
    return default_percent

def get_all_popular_apis():
    query = """MATCH (n)-[r:SUPPORTS]-(a:Api)
    RETURN a.name, COUNT(r) ORDER BY COUNT(r) DESC LIMIT 3"""
    results, meta = db.cypher_query(query)
    apis = [r[0] for r in results]
    return apis


def get_all_popular_transactions_methods():
    query = """MATCH (same)-[r]-(t:TransactionMethod)
    RETURN t.name, COUNT(r) ORDER BY COUNT(r) DESC LIMIT 3"""
    results, meta = db.cypher_query(query)
    methods = [r[0] for r in results]
    return methods


def get_all_popular_consistency_methods():
    query = """MATCH (same)-[r]-(c:ConsistencyMethod)
    RETURN c.name, COUNT(r) ORDER BY COUNT(r) DESC LIMIT 3"""
    results, meta = db.cypher_query(query)
    methods = [r[0] for r in results]
    return methods


def find_datamodel_count():
    query = """MATCH (d:DataModel)
    RETURN d.name as name, SIZE((:NoSQL)-[:TYPE]-(d)) as count"""
    results, meta = db.cypher_query(query)
    datamodels = []
    for d in results:
        datamodels.append({'name': d[0], 'count': d[1]})
    return datamodels


def filter_all_nosqls(datamodels, languages):
    query = """MATCH (da:DataModel)-[r]-(n:NoSQL)-[:SAME_AS]-(other)-[:HAS]-(d:Driver)-[:FOR]-(p:ProgrammingLanguage)
    WHERE p.uri IN {} AND da.uri IN {}
    RETURN DISTINCT n""".format(languages, datamodels)
    results, meta = db.cypher_query(query)
    nosqls = [NoSQL.inflate(row[0]) for row in results]
    return nosqls


def filter_nosqls_by_movement(nosqls, movements):
    nosqls = [n.uri for n in nosqls]
    query = """MATCH (n:NoSQL)-[:LICENSED_UNDER]-(l:License)-[:APPROVED_BY]-(m:Movement)
    WHERE m.name IN {movements} AND n.uri in {nosqls} RETURN n as nosql
    UNION
    MATCH (n2:NoSQL)-[:SAME_AS]-(other)-[:APPROVED_BY]-(m2:Movement)
    WHERE m2.name IN {movements} AND n2.uri in {nosqls} RETURN n2 as nosql"""
    query = query.format(movements=movements, nosqls=nosqls)
    results, meta = db.cypher_query(query)
    results_nosqls = [NoSQL.inflate(row[0]) for row in results]
    return results_nosqls

def get_nosqls(languages, datamodels, movements):
    filter = HomepageFilter()
    print(movements)
    results = list(filter.filter(movements=movements, datamodels=datamodels, languages=languages))
    clean_results = []
    if results:
        max_count = max([r['count'] for r in results])
        for n in results:
            percent = 0
            if n['count'] > 0:
                percent = n['count'] * 100 / max_count
            n['percent'] = percent
            clean_results.append(percent)
    return clean_results

def get_nosql_by_name(name):
    query = """
    MATCH (n:NoSQL) WHERE toLower(n.name) CONTAINS toLower('{name}')
    OPTIONAL MATCH (re:Review)-[:FOR]-(n)
    OPTIONAL MATCH (n)-[:SAME_AS]-(sames) 
    WITH n, COLLECT(sames.description) as descriptions, re, COUNT(DISTINCT sames) as counter
    RETURN DISTINCT n.uri, n.name, n.thumbnail, n.description, 
    FILTER(i in descriptions WHERE size(i) > 1), re.rating, counter 
    ORDER BY counter DESC
    """.format(name=name)
    results, meta = db.cypher_query(query)
    nosqls = []
    if results:
        max_count = max([r[6] for r in results])
        for r in results:
            data = {}
            data['uri'] = r[0]
            data['name'] = r[1]
            data['thumbnail'] = r[2]
            data['description'] = r[3]
            data['alternative_descriptions'] = r[4]
            data['rating'] = r[5]
            data['count'] = r[6]
            if data['count'] > 0:
                percent = data['count'] * 100 / max_count
            else:
                percent = 0
            data['percent'] = percent
            nosqls.append(data)
    return nosqls


def get_nosql_by_datamodel(name):
    query = """MATCH (n:NoSQL)-[:TYPE]-(d:DataModel)
    OPTIONAL MATCH (n)-[:SAME_AS]-(sames) 
    OPTIONAL MATCH (re:Review)-[:FOR]-(n) 
    WITH n, d, COLLECT(sames.description) AS descriptions, COUNT(DISTINCT sames) as counter, re 
    WHERE d.name = '{}' WITH n, d, descriptions, counter,re
    RETURN DISTINCT n.uri, n.name, n.thumbnail, n.description, 
    FILTER(i in descriptions WHERE length(i) > 1) ,re.rating, counter 
    ORDER BY counter DESC""".format(name)
    results, meta = db.cypher_query(query)
    nosqls = []
    max_count = max([r[6] for r in results])
    for r in results:
        data = {}
        data['uri'] = r[0]
        data['name'] = r[1]
        data['thumbnail'] = r[2]
        data['description'] = r[3]
        data['alternative_descriptions'] = r[4]
        data['rating'] = r[5]
        data['count'] = r[6]
        if data['count'] > 0:
            percent = data['count'] * 100 / max_count
        else:
            percent = 0
        data['percent'] = percent
        nosqls.append(data)
    return nosqls

def get_similar_by_datamodel(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:TYPE]-(d:DataModel)-[:TYPE]-(n2:NoSQL) WITH n2
    MATCH (n2)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year) WITH n2, y ORDER BY y.year
    OPTIONAL MATCH (n2)-[:SAME_AS]-(db:DBEnginePost)
    OPTIONAL MATCH (n2)-[:SAME_AS]-(al:AlternativetoPost)
    OPTIONAL MATCH (n2)-[:SAME_AS]-(st:StacksharePost)
    OPTIONAL MATCH (n2)-[:SAME_AS]-(stack:StackoverflowTag)
    OPTIONAL MATCH (n2)-[:HAS]-(re:ClusterTechsRepositories)
    OPTIONAL MATCH (n2)-[:HAS]-(rev:Review)
    RETURN n2.uri, n2.name, db.score, al.likes, st.votes, stack.views,
    stack.number_questions, re.total, rev.total_rating, rev.avg_sentiment, COLLECT(y)""".format(uri)
    results, meta = db.cypher_query(query)

    similars = []
    for r in results:
        data = {}
        data['uri'] = r[0]
        data['name'] = r[1]
        data['dbengine_rank'] = utils.get_valid_int(r[2])
        data['alternativeto_likes'] = utils.get_valid_int(r[3])
        data['stackshare_votes'] = utils.get_valid_int(r[4])
        data['stackoverflow'] = utils.get_valid_int(r[5])
        data['stackoverflow_questions'] = utils.get_valid_int(r[6])
        data['repositories'] = utils.get_valid_int(r[7])
        data['rating'] = r[8]
        data['sentiment'] = r[9]
        data['interest_over_time'] = r[10]
        similars.append(data)
    return similars


def get_similar_by_stackoverflow(uri):
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(:StackoverflowTag)-[:RELATED_TO]-(tag:StackoverflowTag),
    (they:NoSQL)-[:SAME_AS]-(:StackoverflowTag)-[:RELATED_TO]-(tag)
    WHERE n.uri = '{}' AND n <> they
    WITH they 
    MATCH (they)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year) WITH they, y ORDER BY y.year
    OPTIONAL MATCH (they)-[:SAME_AS]-(db:DBEnginePost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(al:AlternativetoPost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(st:StacksharePost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(stack:StackoverflowTag)
    OPTIONAL MATCH (they)-[:HAS]-(re:ClusterTechsRepositories)
    OPTIONAL MATCH (they)-[:HAS]-(rev:Review)
    RETURN they.uri, they.name, db.global_rank, al.likes, st.votes, stack.views, 
    stack.number_questions, re.total, rev.total_rating, rev.avg_sentiment, COLLECT(DISTINCT y)""".format(uri)
    results, meta = db.cypher_query(query)
    similars = []
    for r in results:
        data = {}
        data['uri'] = r[0]
        data['name'] = r[1]
        data['dbengine_rank'] = utils.get_valid_int(r[2])
        data['alternativeto_likes'] = utils.get_valid_int(r[3])
        data['stackshare_votes'] = utils.get_valid_int(r[4])
        data['stackoverflow'] = utils.get_valid_int(r[5])
        data['stackoverflow_questions'] = utils.get_valid_int(r[6])
        data['repositories'] = utils.get_valid_int(r[7])
        data['rating'] = r[8]
        data['sentiment'] = r[9]
        data['interest_over_time'] = r[10]
        similars.append(data)
    return similars


def get_similar_by_performance(uri):
    query = """MATCH (n:NoSQL)-[:HAS]-(re:Review)
    WHERE n.uri = '{}' WITH re
    MATCH (n:NoSQL)-[:HAS]-(re2:Review)
    WHERE re2.avg_sentiment >= round(100 * re.avg_sentiment) / 100 
    WITH n, re2
    MATCH (n)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
    WITH n, y, re2 ORDER BY y.year
    OPTIONAL MATCH (n)-[:SAME_AS]-(db:DBEnginePost)
    OPTIONAL MATCH (n)-[:SAME_AS]-(al:AlternativetoPost)
    OPTIONAL MATCH (n)-[:SAME_AS]-(st:StacksharePost)
    OPTIONAL MATCH (n)-[:SAME_AS]-(stack:StackoverflowTag)
    OPTIONAL MATCH (n)-[:HAS]-(re:ClusterTechsRepositories)
    RETURN n.uri, n.name, db.global_rank, al.likes, st.votes,
    stack.views, stack.number_questions, re.total, re2.total_rating, 
    re2.avg_sentiment, COLLECT(y)""".format(uri)
    results, meta = db.cypher_query(query)
    similars = []
    for r in results:
        data = {}
        data['uri'] = r[0]
        data['name'] = r[1]
        data['dbengine_rank'] = utils.get_valid_int(r[2])
        data['alternativeto_likes'] = utils.get_valid_int(r[3])
        data['stackshare_votes'] = utils.get_valid_int(r[4])
        data['stackoverflow'] = utils.get_valid_int(r[5])
        data['stackoverflow_questions'] = utils.get_valid_int(r[6])
        data['repositories'] = utils.get_valid_int(r[7])
        data['rating'] = utils.get_valid_int(r[8])
        data['sentiment'] = r[9]
        data['interest_over_time'] = r[10]
        similars.append(data)
    return similars


def get_supported_languages_uris(uri):
    query = """MATCH (n:NoSQL {{uri: '{0}'}})-[:SUPPORTS]-(p:ProgrammingLanguage) 
    RETURN p.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:SAME_AS]-(same:StackoverflowTag)-[:SUPPORTS]-(p:ProgrammingLanguage)
    RETURN p.name
    UNION
    MATCH (n:NoSQL {{uri: '{0}'}})-[:HAS]-(dc:ClusterTechsRepositories)-[r:HAS]-(p:ProgrammingLanguage)
    WHERE r.total > 0 RETURN p.name""".format(uri)
    results, meta = db.cypher_query(query)
    languages = []
    for r in results:
        languages.append(r[0])
    return languages


def get_similar_by_languages(uri, languages):
    query = """MATCH (they:NoSQL)-[:SAME_AS]-(same2)-[:SUPPORTS]-(p)
    WHERE they.dbpedia_uri <> '{uri}' AND p.name IN {languages}
    WITH they, p
    MATCH (they)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
    WITH they, y, p ORDER BY y.year
    OPTIONAL MATCH (they)-[:SAME_AS]-(db:DBEnginePost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(al:AlternativetoPost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(st:StacksharePost)
    OPTIONAL MATCH (they)-[:SAME_AS]-(stack:StackoverflowTag)
    OPTIONAL MATCH (they)-[:HAS]-(re:ClusterTechsRepositories)
    OPTIONAL MATCH (they)-[:HAS]-(rev:Review)
    RETURN they.uri, they.name, db.global_rank, al.likes, st.votes, stack.views, 
    stack.number_questions, re.total, rev.total_rating, rev.avg_sentiment, 
    COLLECT(DISTINCT p.name), size(COLLECT( DISTINCT p.name)), 
    COLLECT(DISTINCT y) ORDER BY size(COLLECT( DISTINCT p.name)) DESC""".format(uri=uri, languages=languages)
    results, meta = db.cypher_query(query)
    similars = []
    for r in results:
        data = {}
        data['uri'] = r[0]
        data['name'] = r[1]
        data['dbengine_rank'] = utils.get_valid_int(r[2])
        data['alternativeto_likes'] = utils.get_valid_int(r[3])
        data['stackshare_votes'] = utils.get_valid_int(r[4])
        data['stackoverflow'] = utils.get_valid_int(r[5])
        data['stackoverflow_questions'] = utils.get_valid_int(r[6])
        data['repositories'] = utils.get_valid_int(r[7])
        data['rating'] = r[8]
        data['sentiment'] = r[9]
        data['languages'] = r[10]
        data['number_languages'] = r[11]
        data['interest_over_time'] = r[12]
        similars.append(data)
    return similars


def find_language_stats(uri):
    query = """MATCH (n:NoSQL {{uri:'{}'}})-[:HAS]-(dc:ClusterTechsRepositories)-[r:HAS]-(p:ProgrammingLanguage) 
    RETURN p.name, r.total ORDER BY r.total DESC LIMIT 5""".format(uri)
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'count': r[1]} for r in results]
    total = sum([r['count'] for r in results])
    for l in results:
        if l['count'] <= 0:
            percent = 0
        else:
            percent = int(l['count'] * 100 / total)
        l['percentage'] = percent
    return results


def get_repos_by_language(nosql_uri, language_name):
    query = """MATCH (n:NoSQL {{uri:'{nosql_uri}'}})-[:HAS]-(dr:DriverReposCluster)-[:FOR]-(p:ProgrammingLanguage {{name:'{lang_name}'}}) 
    WITH dr
    MATCH (dr)-[:HAS]-(re:Repository) 
    RETURN re.name, re.summary, re.has_wiki, re.has_downloads, re.stars"""
    query = query.format(nosql_uri=nosql_uri, lang_name=language_name)
    results, meta = db.cypher_query(query)
    repositories = []
    for r in results:
        repo = {}
        repo['name'] = r[0]
        repo['description'] = r[1]
        repo['has_wiki'] = r[2]
        repo['has_downloads'] = r[3]
        repo['stars'] = r[4]
        repositories.append(repo)
    return repositories


def find_sentiments_percents(uri):
    positive_count = find_count_positive_comments(uri)
    neutral_count = find_count_neutral_comments(uri)
    negative_count = find_count_negative_comments(uri)
    total = find_total_comments(uri)

    if total > 0:
        positive_percent = int((positive_count * 100) / total)
        neutral_percent = int((neutral_count * 100) / total)
        negative_percent = int((negative_count * 100) / total)
    else:
        positive_percent = 0
        neutral_percent = 0
        negative_percent = 0
    return {'positive': positive_percent, 'neutral': neutral_percent,
            'negative': negative_percent, 'total': total}


def find_count_positive_comments(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(re:Review)-[:HAS]-(c:Comment) 
    WHERE c.sentiment > 0.1 RETURN COUNT(c)""".format(uri)
    results, meta = db.cypher_query(query)
    result = results[0][0]
    return result


def find_count_neutral_comments(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(:Review)-[:HAS]-(c:Comment)
    WHERE c.sentiment < 0.1 AND c.sentiment > 0.02
    RETURN COUNT(c)""".format(uri)
    results, meta = db.cypher_query(query)
    result = results[0][0]
    return result


def find_count_negative_comments(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(:Review)-[:HAS]-(c:Comment)
    WHERE c.sentiment < 0.02
    RETURN COUNT(c)""".format(uri)
    results, meta = db.cypher_query(query)
    result = results[0][0]
    return result


def find_total_comments(uri):
    query = """MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(:Review)-[:HAS]-(c:Comment)
    RETURN COUNT(c)""".format(uri)
    results, meta = db.cypher_query(query)
    result = results[0][0]
    return result


def get_all_stackoverflow_views():
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(st:StackoverflowTag) 
    RETURN n.name, st.views"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'views': r[1]} for r in results]
    return results


def get_all_dbengine_scores():
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(db:DBEnginePost) 
    RETURN n.name, db.score"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'score': r[1]} for r in results if r[1]]
    return results

def get_all_repositories_stars():
    query = """MATCH (n:NoSQL)-[:HAS]-(re:Repository)
    RETURN n.name, re.stars"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'stars': r[1]} for r in results]
    return results

def get_all_stackshare_votes():
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(st:StacksharePost)
    RETURN n.name, st.votes"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'votes': r[1]} for r in results]
    return results

def get_all_alternativeto_likes():
    query = """MATCH (n:NoSQL)-[:SAME_AS]-(al:AlternativetoPost)
    RETURN n.name, al.likes"""
    results, meta = db.cypher_query(query)
    results = [{'name': r[0], 'likes': r[1]} for r in results]
    return results
