import unicodedata
import re

def get_first_or_none(array):
    result = None
    if array:
        result = array[0]
    return result


def get_valid_int(value):
    if value:
        return value
    else:
        return 0


def to_int(number):
    number = number.replace(',', '')
    return int(number)


def number_format(value, num_decimals=2):
    int_value = int(value)
    formatted_number = '{{:.{}f}}'.format(num_decimals)
    if int_value < 1000:
        return str(int_value)
    elif int_value < 1000000:
        return formatted_number.format(int_value/1000.0).rstrip('0.') + 'K'
    else:
        return formatted_number.format(int_value/1000000.0).rstrip('0.') + 'M'


def percentage_difference(data):
    data = [d['queries'] for d in data]
    try:
        new_number = data[-1]
        original_number = data[-2]
        increase = new_number - original_number
        increase_percent = round((increase / original_number) * 100)
    except:
        increase_percent = 0
    return abs(increase_percent)


def get_sentiment_percent_range(value):
    percent = (value - (-1)) / 2
    positive = int(percent * 100)
    negative = 100 - positive
    return {'positive': positive, 'negative': negative}


def get_sentiment_category(sentiment):
    category = 'negative'
    if sentiment > 0.1:
        category = 'positive'
    elif sentiment < 0.1 and sentiment > 0.02:
        category = 'neutral'
    return category


def remove_accents(s):
	clean_s = ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
	return clean_s

def remove_parenthesis(name):
	name = re.sub('_\(.*\)', '', name)
	return name

def remove_whitespace(name):
	name = re.sub('\s', '-', name)
	return name

def remove_points(name):
	name = re.sub('\.', '', name)
	return name

def remove_commas(name):
	name = name.replace(',', '')
	return name

def remove_two_points(name):
	name = name.replace('::', '-')
	return name

def clean_uri(name):
	name = remove_parenthesis(name)
	name = remove_whitespace(name)
	name = remove_points(name)
	name = remove_commas(name)
	name = remove_two_points(name)
	return name