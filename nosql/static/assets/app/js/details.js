$(document).ready(function() {

  // Main Comments CHart
  if($("#count-comments-chart").length) {
      years = $("#count-comments-chart").data('years').split(',');
      comments = String($("#count-comments-chart").data('comments')).split(',');
      commentData = [];
      for(i=0; i < years.length; i++){
        commentData.push({'year': years[i], 'comment': comments[i]});
      }
      var groupedByYear = _.groupBy(commentData, function(item) {
          return item.year.substring(0,4);
      });
      values = _.map(groupedByYear, function(items){ return items.length });
      labels = Object.keys(groupedByYear);
      var lineData = {
        labels: labels,
        datasets: [
            {
                label: "Comments",
                borderColor: 'rgba(24,197,169,0.7)',
                backgroundColor: 'rgba(24,197,169,0.5)',
                pointBackgroundColor: 'rgba(24,197,169,1)',
                pointBorderColor: "#fff",
                data: values,
            }
        ],
      };
      var lineOptions = {
          responsive: true,
          maintainAspectRatio: false,

          showScale: false,
          scales: {
              xAxes: [{
                  gridLines: {
                      display: false,
                  },
              }],
              yAxes: [{
                  gridLines: {
                      display: false,
                      drawBorder: false,
                  },
                  ticks: {
                      beginAtZero: true
                  }
              }]
          },
          legend: {display: false}
      };
      var ctx = document.getElementById("count-comments-chart").getContext("2d");
      if (values.length == 1){
        type = 'bar';
      } else {
        type = 'line';
      }
      new Chart(ctx, {type: type, data: lineData, options:lineOptions});
  }


  // Interest Over Time Mini Chart
  if ($("#interest-over-time-mini-chart").length){
    var interestData = {
        labels: interestOverTimeLabels,
        datasets: [
            {
                label: "Google Queries",
                backgroundColor: '#2CC4CB', // '#30C8B3'
                borderColor: "#fff",
                data: interestOverTimeQueries
            }
        ]
    };
    var interestOptions = {
        responsive: true,
        maintainAspectRatio: false,

        showScale: false,
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                  callback: function(value) { 
                    return value.substr(-2); 
                  }
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                    drawTicks:false,
                    tickMarkLength: 0
                },
                ticks: {
                    display: false,
                    padding: 0,
                },
            }]
        },
        legend: {display: false}
    };

    var ctxInterest = document.getElementById("interest-over-time-mini-chart").getContext("2d");
    new Chart(ctxInterest, {type: 'bar', data: interestData, options:interestOptions});
  }

  // Comments Sentiment By Year Mini Chart
  if($("#comments-sentiment-mini-chart").length){
    // Comments Sentiments Mini Chart
    var groupedByYear = _.groupBy(commentsSentimentsData, function(item) {
      return item.created_at.substring(0,4);
    });

    positivesData = [];
    negativesData = [];

    _.map(groupedByYear, function(items){ 
      positives = 0;
      negatives = 0;
      for(i=0; i < items.length; i++){
        sentiment = items[i].sentiment;
        if(sentiment < 0.1 ){
          negatives++;
        } else {
          positives++;
        }
      }
      negativesData.push(negatives);
      positivesData.push(positives);
    });

    commentLabels = Object.keys(groupedByYear)
    
    var commentsData = {
        labels: commentLabels,
        datasets: [
            {
                label: "Positive Comments",
                backgroundColor:'#2CC4CB', //'rgba(220, 220, 220, 0.5)',
                data: positivesData
            },
            {
                label: "Negative Comments",
                backgroundColor: '#FC6180', // '#30C8B3'
                borderColor: "#fff",
                data: negativesData
            }
        ]
    };
    var commentsOptions = {
        responsive: true,
        maintainAspectRatio: false,

        showScale: false,
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                  callback: function(value) { 
                    return value.substr(-2); 
                  }
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                    drawTicks:false,
                    tickMarkLength: 0
                },
                ticks: {
                    display: false,
                    padding: 0,
                },
            }]
        },
        legend: {display: false}
    };

    var ctxComments = document.getElementById("comments-sentiment-mini-chart").getContext("2d");
    new Chart(ctxComments, {type: 'bar', data: commentsData, options: commentsOptions});
  }

  // Comments By Sentiment Mini Chart
  if($("#donut-comments-mini-chart").length){
    Chart.defaults.global.legend.labels.usePointStyle = true;
    var ctxDonutComments = document.getElementById('donut-comments-mini-chart');
    var commentsDonutData = {
      labels:["Positive","Negative","Neutral"],
      datasets:[
        {
          data:[positivePercent, negativePercent, neutralPercent],
          backgroundColor: [
            "#28d8b2",
            "#FC6180",
            "#1880C9"
          ],
          borderWidth:["2px","2px","2px","2px"],
          borderColor:["#FFf","#fff","#fff"]
        }
      ],
    };
    var commentsDonutOptions = {
          legend: {
              display: true,
              position: 'left',
              labels: {
                  fontColor: '#333',
                  usePointStyle: true,
              }
          },
          cutoutPercentage: 70
      };
    new Chart(ctxDonutComments,{type:'doughnut', data:commentsDonutData, options: commentsDonutOptions});
  }

  // Questions in Stack Overflow Mini Chart
  if($("#donut-stackoverflow-mini-chart").length){  
    var ctxQuestions = document.getElementById('donut-stackoverflow-mini-chart');
    var questionsData = {
      labels:["Unanswered","Answered"],
      datasets:[
        {
          data:[unansweredQuestions, answeredQuestions],
          backgroundColor: [
            "#FC6180",
            "#28D8B2"
          ],
          borderWidth:["2px","2px","2px","2px"],
          borderColor:["#FFf","#fff","#fff"]
        }
      ],
    };
    var questionsOptions = {
          legend: {
              display: true,
              position: 'left',
              labels: {
                  fontColor: '#333',
                  usePointStyle: true
              }
          },
          cutoutPercentage: 70
      };
    new Chart(ctxQuestions,{type:'doughnut', data: questionsData, options: questionsOptions});
  }
});

