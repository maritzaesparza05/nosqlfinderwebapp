$(document).ready(function() {

$('#comment-text').slimScroll({
        height: '200px'
});

var drawMiniPie = function(element, e, data) {
    var container = d3.select(element),
    r = e / 2 - 2,
    i = d3.sum(data, function(t) {
        return t.value
    }),
    s = d3.tip().attr("class", "d3-tip").offset([-10, 0]).direction("e").html(function(t) {
        return "<ul class='list-unstyled mb-5'><li><div class='text-size-base mb-5 mt-5'>" + t.data.icon + t.data.sentiment + "</div></li><li>Value: &nbsp;<span class='text-semibold pull-right'>" + t.value + "</span></li></ul>"
    }),
    l = container.append("svg").call(s),
    o = l.attr("width", e).attr("height", e).append("g").attr("transform", "translate(" + e / 2 + "," + e / 2 + ")"),
    d = d3.layout.pie().sort(null).startAngle(Math.PI).endAngle(3 * Math.PI).value(function(t) {
        return t.value
    }),
    c = d3.svg.arc().outerRadius(r).innerRadius(r / 2),
    u = o.selectAll(".d3-arc").data(d(data)).enter().append("g").attr("class", "d3-arc").style("stroke", "#fff").style("cursor", "pointer"),
    p = u.append("path").style("fill", function(t) {
        return t.data.color
    });
    p.on("mouseover", function(t, e) {
        d3.select(this).transition().duration(500).ease("elastic").attr("transform", function(t) {
            return t.midAngle = (t.endAngle - t.startAngle) / 2 + t.startAngle, "translate(" + 2 * Math.sin(t.midAngle) + "," + 2 * -Math.cos(t.midAngle) + ")"
        })
    }).on("mousemove", function(t) {
        s.show(t).style("top", d3.event.pageY - 40 + "px").style("left", d3.event.pageX + 30 + "px")
    }).on("mouseout", function(t, e) {
        d3.select(this).transition().duration(500).ease("bounce").attr("transform", "translate(0,0)"), s.hide(t)
    }), p.transition().delay(function(t, e) {
        return 500 * e
    }).duration(500).attrTween("d", function(t) {
        var e = d3.interpolate(t.startAngle, t.endAngle);
        return function(a) {
            return t.endAngle = e(a), c(t)
        }
    })
};

var drawSentimientMiniPies = function(){
    var pies = $('[data-toggle="data-pie"]');
    for (i=0; i < pies.length; i++) {
        var data = [{
            sentiment: "Positive",
            icon: "<i class='fa fa-smile-o position-left'></i>",
            value: parseInt($(pies[i]).attr('positive')),
            color: "#2ABCC3"
        }, {
            sentiment: "Negative",
            icon: "<i class='fa fa-frown-o position-left'></i>",
            value: parseInt($(pies[i]).attr('negative')),
            color: "#FC6180"
        }];
        drawMiniPie($(pies[i]).get(0), 42, data);
    }
};

drawSentimientMiniPies();

$('#comments-table').DataTable();
});

