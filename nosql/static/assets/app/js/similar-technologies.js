// Comparision by datamodel
// ------------------------------
$("#similar-datamodel").click(function(){
    var url = '/nosql/api/similar/datamodel';
    var container = $("#comparision-table");
    $.ajax(
        {
          url: url,
          data: {
             'uri': nosqlUri
          },
          beforeSend: function(){
            $("#comparision-table").html('<div class="preloader3 loader-block">' +
            '<div class="circ1 loader-cyan"></div>' + 
            '<div class="circ2 loader-cyan"></div>' +
            '<div class="circ3 loader-cyan"></div>' +
            '<div class="circ4 loader-cyan"></div>');
          },
          success: function (data) {
             $("#comparision-table").html(data);
             $('[data-toggle="data-table"]').DataTable();
             $('#compare-title').text('Compare By Data Model');
             explanation = "The comparison is based on NoSQLs who share the same Data Models";
             $('#table-explanation').html('<i class="icofont icofont-question-circle f-20 text-cyan p-r-5"></i>' + explanation);
          }
        }
    );
});

// Comparision by stackoverlow tags
// ------------------------------
$("#similar-stackoverflow-tags").click(function(){
    var url = '/nosql/api/similar/stackoverflow';
    var container = $("#comparision-table");
    $.ajax(
        {
          url: url,
          data: {
             'uri': nosqlUri
          },
          beforeSend: function(){
            $("#comparision-table").html('<div class="preloader3 loader-block">' +
            '<div class="circ1 loader-cyan"></div>' + 
            '<div class="circ2 loader-cyan"></div>' +
            '<div class="circ3 loader-cyan"></div>' +
            '<div class="circ4 loader-cyan"></div>');
          },
          success: function (data) {
              $("#comparision-table").html(data);
              $('[data-toggle="data-table"]').DataTable();
              $('#compare-title').text('Compare By Stack Overflow Tags');
              explanation = "The comparison is based on NoSQLs who shares technologies in Stack Overflow questions";
              $('#table-explanation').html('<i class="icofont icofont-question-circle f-20 text-cyan p-r-5"></i>' + explanation);
          }
        }
    );
});

// Comparision by similar languages
// ------------------------------
$("#similar-languages").click(function(){
    var url = '/nosql/api/similar/language';
    var container = $("#comparision-table");
    $.ajax(
        {
          url: url,
          data: {
             'uri': nosqlUri
          },
          beforeSend: function(){
            $("#comparision-table").html('<div class="preloader3 loader-block">' +
            '<div class="circ1 loader-cyan"></div>' + 
            '<div class="circ2 loader-cyan"></div>' +
            '<div class="circ3 loader-cyan"></div>' +
            '<div class="circ4 loader-cyan"></div>');
          },
          success: function (data) {
              $("#comparision-table").html(data);
              $('[data-toggle="data-table"]').DataTable();
              $('#compare-title').text('Compare By Languages');
              explanation = "The comparison is based on NoSQLs who supports the same programming languages";
              $('#table-explanation').html('<i class="icofont icofont-question-circle f-20 text-cyan p-r-5"></i>' + explanation);
          }
        }
    );
});

// Comparision by similar performance
// ------------------------------
$("#similar-performance").click(function(){
    var url = '/nosql/api/similar/performance';
    var container = $("#comparision-table");
    $.ajax(
        {
            url: url,
            data: {
               'uri': nosqlUri
            },
            beforeSend: function(){
                $("#comparision-table").html('<div class="preloader3 loader-block">' +
                  '<div class="circ1 loader-cyan"></div>' + 
                  '<div class="circ2 loader-cyan"></div>' +
                  '<div class="circ3 loader-cyan"></div>' +
                  '<div class="circ4 loader-cyan"></div>');
            },
            success: function (data) {
                $("#comparision-table").html(data);
                $('[data-toggle="data-table"]').DataTable();
                $('#compare-title').text('Compare By Performance');
                explanation = "The comparison is based on NoSQLs who share a similar performance";
                $('#table-explanation').html('<i class="icofont icofont-question-circle f-20 text-cyan p-r-5"></i>' + explanation);
            }
        }
    );
});


// Draw Sparklines and Ratings
$('#comparision-table').on( 'draw.dt', function () {

  canvas = $('[data-toggle="sparkline"]').not('.done');
  for(i=0; i < canvas.length; i++){
  yearStr = $(canvas[i]).data('years');
  interestStr = $(canvas[i]).data('interest');
  
  years = yearStr.split(",");
  years = _.map(years, Number);

  interest = interestStr.split(",");
  interest = _.map(interest, Number);

  var filledLineChart = $(canvas[i]).get(0).getContext('2d');
  primaryGradient = filledLineChart.createLinearGradient(0, 0, 0, 70);
  primaryGradient.addColorStop(0, 'rgba(40,216,178,0.5)');
  primaryGradient.addColorStop(1, 'rgba(40,216,178,0)');
  var filledLineData = {
                labels: years,
                datasets: [{
                    label: "Google Queries",
                    fill: true,
                    backgroundColor: primaryGradient,
                    borderColor: 'rgba(40,216,178,0.5)',
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: 'rgba(40,216,178,0.5)',
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: 'rgba(40,216,178,0.5)',
                    pointHoverBorderColor: 'rgba(40,216,178,0.5)',
                    pointHoverBorderWidth: 0,
                    pointRadius: 2,
                    pointHitRadius: 10,
                    data: interest
                }]
  };
  $(canvas[i]).addClass('done');

  // line chart init
  var filledLineChart = new Chart(filledLineChart, {
      type: 'line',
      data: filledLineData,
      options: {
          legend: {
              display: false
          },
          scales: {
              xAxes: [{
                display: false,
                  ticks: {
                      fontSize: '10',
                      fontColor: '#a5b5c5'
                  },
                  gridLines: {
                      color: '#f1f1f1',
                      zeroLineColor: '#f1f1f1'
                  }
              }],
              yAxes: [{
                display: false,
                  ticks: {
                      fontSize: '10',
                      fontColor: '#a5b5c5'
                  },
                  gridLines: {
                      color: 'transparent',
                      zeroLineColor: 'transparent'
                  }
              }]
          }
      }
  });
}

  $('[data-toggle="sparkline"]:not(:has(canvas))').sparkline('html', {
  //     width:"100",
  //     height:"35",
  //     barColor:"#4d8cf4",
  //     fillColor:!1,
  //     spotColor:!1,
  //     minSpotColor:!1,
  //     maxSpotColor:!1,
  //     lineWidth:1.15
  });

  elements = $('[data-toggle="rating"]');
  for(i=0; i < elements.length; i++){
      currentRating = $(elements[i]).data('current-rating');
      $(elements[i]).barrating({
          theme: 'fontawesome-stars-o',
      initialRating: currentRating
      });
      $(elements[i]).barrating('readonly', true);
  }
});

    
var stackoverflowSimilar = AmCharts.makeChart("recommended-questions-stackoverflow-chart", {
  "type": "serial",
  "theme": "light",
  "hideCredits": true,
  "dataProvider": similarByStackoverflow,
  "startDuration": 1,
  "categoryField": "name",
  "autoMarginOffset":20,
  "categoryAxis": {
      "autoGridCount": true,
      "gridAlpha": 0.1,
      "gridColor": "#FFFFFF",
      "axisColor": "#555555",
      "labelsEnabled": false,
      "color": "#97999b",
  },
  "valueAxes": [{
      "id": "a1",
      "title": "Ratio",
      "gridAlpha": 0,
      "axisAlpha": 0
  }],
  "graphs": [{
      "id": "g1",
      "valueField": "value",
      "title": "distance",
      "type": "column",
      "fillAlphas": 0.9,
      "valueAxis": "a1",
      "balloonText": "[[value]] ratio",
      "legendValueText": "[[value]] mi",
      "legendPeriodValueText": "total: [[value.sum]] mi",
      "lineColor": "#28D8B2",
      "alphaField": "alpha",
      "color": "#fff"
  }],
   "chartCursor": {
      "cursorAlpha": 0.1,
      "cursorColor":"#28D8B2",
       "fullWidth":true,
      "valueBalloonsEnabled": true,
      "zoomable": true
  },
  "chartScrollbar": {
      "graph": "g1",
      "oppositeAxis":false,
      "offset":30,
      "scrollbarHeight": 50,
      "backgroundAlpha": 0,
      "selectedBackgroundAlpha": 0.1,
      "selectedBackgroundColor": "#888888",
      "graphFillAlpha": 0,
      "graphLineAlpha": 0.5,
      "selectedGraphFillAlpha": 0,
      "selectedGraphLineAlpha": 1,
      "autoGridCount":false,
      "color":"#AAAAAA",
      "maximum": 0.4
  }    
});

// this method is called when chart is first inited as we listen for "rendered" event
// function zoomChart() {
//   stackoverflowSimilar.zoomToIndexes(0, 4);
// }
// stackoverflowSimilar.addListener("rendered", zoomChart);
// zoomChart();



