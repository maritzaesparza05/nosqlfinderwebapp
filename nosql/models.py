from django.db import models
from django.core.urlresolvers import reverse

from neomodel import StructuredNode, StringProperty, RelationshipTo, IntegerProperty, StructuredRel, FloatProperty
from neomodel import db

from .services.utils import remove_accents, clean_uri
from .services import utils
from .nosql_queries import apis_calls


class QualityAttribute(StructuredNode):
    uri = StringProperty()
    name = StringProperty()


class Comment(StructuredNode):
    url = StringProperty()
    text = StringProperty(unique_index=True)
    rating = FloatProperty()
    sentiment = FloatProperty()


class Review(StructuredNode):
    total = IntegerProperty()
    rating = FloatProperty()
    average_sentiment = FloatProperty()
    comments = RelationshipTo('Comment', 'CONTAINS')
    nosql = RelationshipTo('NoSQL', 'FOR')
    quality_attribute = RelationshipTo('QualityAttribute', 'FOR')


class DataModel(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    description = StringProperty()
    thumbnail = StringProperty()


class NoSQL(StructuredNode):
    uri = StringProperty(unique_index=True)
    dbpedia_uri = StringProperty()
    wikidata_id = StringProperty()
    name = StringProperty()
    description = StringProperty()
    wikipedia_summary = StringProperty()
    thumbnail = StringProperty()
    homepage = StringProperty()
    status = StringProperty()
    frequently_updated = StringProperty()
    latest_release_version = StringProperty()
    latest_release_date = StringProperty()
    twitter = StringProperty()
    quora_topic = StringProperty()
    datamodel = RelationshipTo('DataModel', 'TYPE')
    links = RelationshipTo('Link', 'CONTAINS')
    licenses = RelationshipTo('License', 'LICENSED_UNDER')
    operating_systems = RelationshipTo('OperatingSystem', 'SUPPORTS')
    developed_languages = RelationshipTo('ProgrammingLanguage', 'DEVELOPED_WITH')
    dbengine_post = RelationshipTo('DBEnginePost', 'SAME_AS')
    developer = RelationshipTo('Developer', 'CREATED_BY')
    dbengine_post = RelationshipTo('DBEnginePost', 'SAME_AS')
    alternativeto_post = RelationshipTo('AlternativetoPost', 'SAME_AS')
    stackshare_post = RelationshipTo('StacksharePost', 'SAME_AS')
    wikipedia_post = RelationshipTo('WikipediaPost', 'SAME_AS')
    repository = RelationshipTo('Repository', 'HAS_A')
    stackoverflow_tag = RelationshipTo('StackoverflowTag', 'SAME_AS')
    repositories = RelationshipTo('Repositories', 'HAS')

    def get_clean_uri(self):
        uri = remove_accents(self.uri)
        uri = clean_uri(uri)
        return uri

    def get_absolute_url(self):
        return reverse('nosql:nosql_detail', args=[self.get_clean_uri()])

    def get_description(self):
        description = ''
        if self.description:
            description = self.description
        else:
            query = """MATCH (n:NoSQL)-[:SAME_AS]-(other) 
            WHERE n.name = '{}' AND other.description CONTAINS n.name 
            RETURN other.description LIMIT 1""".format(self.name)
            results, meta = db.cypher_query(query)
            if results:
                description = results[0]
        return description

    def get_stars(self):
        query = """MATCH (n:NoSQL)-[:FOR]-(rev:Review)
            WHERE n.uri = '{uri}'
            RETURN rev.rating""".format(uri=self.uri)
        results, meta = db.cypher_query(query)
        results = results[0]
        rating = utils.get_first_or_none(results)
        if not rating:
            rating = 0
        rating = round(rating, 1)
        return rating

    def get_interest_over_time(self):
        return apis_calls.get_interest_over_time(self.name)


class DataType(StructuredNode):
    name = StringProperty()


class SupportDataTypes(StructuredNode):
    is_supported = StringProperty()
    datatypes = RelationshipTo('DataType', 'SUPPORTS')


class DBEnginePost(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    homepage = StringProperty()
    documentation = StringProperty()
    initial_release = StringProperty()
    supports_sql = StringProperty()
    supports_concurrency = StringProperty()
    supports_durability = StringProperty()
    is_cloud_based = StringProperty()
    supports_xml = StringProperty()
    category_rank = IntegerProperty()
    global_rank = IntegerProperty()
    score = FloatProperty()
    developed_languages = RelationshipTo('ProgrammingLanguage', 'WRITTEN_IN')
    operating_systems = RelationshipTo('OperatingSystem', 'SUPPORTS')
    uses = RelationshipTo('Use', 'USED_FOR')
    developer = RelationshipTo('Developer', 'CREATED_BY')
    consistency_methods = RelationshipTo('ConsistencyMethod', 'SUPPORTS')
    transaction_methods = RelationshipTo('TransactionMethod', 'SUPPORTS')
    license_types = RelationshipTo('Movement', 'APPROVED_BY')
    drivers = RelationshipTo('ProgrammingLanguage', 'SUPPORTS')
    datatypes = RelationshipTo('DataType', 'SUPPORTS')


class Repository(StructuredNode):
    uri = StringProperty(unique_index=True)
    url = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    homepage = StringProperty()
    owner = StringProperty()
    stars = IntegerProperty()
    forks = IntegerProperty()
    open_issues_count = IntegerProperty()
    has_wiki = StringProperty()
    has_downloads = StringProperty()


class DriverRepository(StructuredNode):
    total = StringProperty()
    language = RelationshipTo('ProgrammingLanguage', 'FOR')
    popular_repos = RelationshipTo('Repository', 'CONTAINS')


class Repositories(StructuredNode):
    total = IntegerProperty()
    driver_repositories = RelationshipTo('DriverRepository', 'CONTAINS')


class Feature(StructuredNode):
    name = StringProperty()
    percentage = IntegerProperty()


class AlternativetoPost(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    homepage = StringProperty()
    thumbnail = StringProperty()
    likes = IntegerProperty()
    alternatives = RelationshipTo('AlternativetoPost', 'RELATED_TO')
    operating_systems = RelationshipTo('OperatingSystem', 'SUPPORTS')
    license_types = RelationshipTo('Movement', 'APPROVED_BY')


class StacksharePost(StructuredNode):
    uri = StringProperty(unique_index=True)
    thumbnail = StringProperty()
    summary = StringProperty()
    votes = IntegerProperty()
    fans = IntegerProperty()
    clients = RelationshipTo('Client', 'USED_BY')
    tools = RelationshipTo('Tool', 'USED_WITH')
    comments = RelationshipTo('Comment', 'HAS')


class RelatedTagRel(StructuredRel):
    weight = FloatProperty()


class StackoverflowTag(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    active = StringProperty()
    views = IntegerProperty()
    author = StringProperty()
    created_at = StringProperty()
    number_questions = IntegerProperty()
    unanswered_questions = IntegerProperty()
    related_tags = RelationshipTo('StackoverflowTag', 'RELATED_TO', model=RelatedTagRel)
    links = RelationshipTo('Link', 'CONTAINS')


class WikipediaPost(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    keywords = RelationshipTo('Keyword', 'RELATED_TO')
    drivers = RelationshipTo('ProgrammingLanguage', 'SUPPORTS')

    def get_drivers_languages(self):
        drivers = self.drivers.all()
        languages = []
        for d in drivers:
            languages += d.language.all()
        return languages


class Developer(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    description = StringProperty()
    thumbnail = StringProperty()
    linkedin = StringProperty()
    location = StringProperty()
    number_employees = StringProperty()
    official_website = StringProperty()
    twitter = StringProperty()
    twitter_followers = IntegerProperty()


class Client(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    thumbnail = StringProperty()


class Tool(StructuredNode):
    uri = StringProperty(unique_index=True)
    name = StringProperty()
    thumbnail = StringProperty()


class OperatingSystem(StructuredNode):
    dbpedia_uri = StringProperty(unique_index=True)
    name = StringProperty()
    summary = StringProperty()
    thumbnail = StringProperty()

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(('name', self.name))


class Driver(StructuredNode):
    status = StringProperty()
    language = RelationshipTo('ProgrammingLanguage', 'FOR')


class ProgrammingLanguage(StructuredNode):
    uri = StringProperty(unique_index=True)
    wikidata_id = StringProperty()
    name = StringProperty()
    description = StringProperty()
    homepage = StringProperty()
    thumbnail = StringProperty()
    paradigms = RelationshipTo('ProgrammingParadigm', 'SUPPORTS')
    uses = RelationshipTo('Use', 'USED_BY')
    licenses = RelationshipTo('License', 'LICENSED_UNDER')

    def __eq__(self, other):
        return self.uri == other.uri

    def __hash__(self):
        return hash(('uri', self.uri))


class License(StructuredNode):
    uri = StringProperty(unique_index=True)
    wikidata_id = StringProperty()
    name = StringProperty()
    description = StringProperty()
    type = RelationshipTo('Movement', 'APPROVED_BY')


class Movement(StructuredNode):
    name = StringProperty(unique_index=True)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(('name', self.name))


class ProgrammingParadigm(StructuredNode):
    uri = StringProperty(unique_index=True)
    wikidata_id = StringProperty()
    name = StringProperty()
    description = StringProperty()


class Use(StructuredNode):
    name = StringProperty()


class Link(StructuredNode):
    url = StringProperty(unique_index=True)
    rank = FloatProperty()


class ConsistencyMethod(StructuredNode):
    name = StringProperty()


class TransactionMethod(StructuredNode):
    name = StringProperty()


class API(StructuredNode):
    name = StringProperty()


class Keyword(StructuredNode):
    name = StringProperty()
