from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

from semanticEngine.main_script import launch_spiders

logger = get_task_logger(__name__)

@periodic_task(
    run_every=(crontab('*/15')),
    name='task_launch_spiders',
    ignore_result=True
)
def task_launch_spiders():
    """
    Start to launch the spiders
    """
    launch_spiders()
    logger.info('All the spiders have been launched')
