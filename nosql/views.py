from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.template.loader import render_to_string

from .models import NoSQL, DataModel, Movement
from .forms import EmailPostForm
from .services.ranking import SiteStats
from .services.bulletChartsQueries import  BulletStats
from .services import utils
from . import api

from rest_framework.views import APIView
from rest_framework.response import Response

import csv


def get_index(request):
    sent = False
    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = '{} ({}) send you a message related to the NoSQLFinder project'.format(cd['name'], 
                cd['email'])
            message = '{}'.format(cd['comments'])
            send_mail(subject, message, '<<SENDER_EMAIL>>', ['<<SENDER_EMAIL>>'])
            sent = True
            form = EmailPostForm()
    else:
        form = EmailPostForm()
    return render(request, 'index.html', {'form': form, 'sent': sent})

    

def show_search_form(request):
    return render(request, 'formWizard.html', {})

def save_form(request):
    datamodels_names = request.POST.getlist('datamodel')
    languages_names = request.POST.getlist('language')
    movements_names = request.POST.getlist('movement')

    datamodels = '+'.join(datamodels_names)
    languages = '+'.join(languages_names)
    movements = '+'.join(movements_names)

    return HttpResponseRedirect(reverse('nosql:filter-results',
                                        args=(datamodels,
                                              languages,
                                              movements, 0)))


def filter_results(request, datamodels, languages, movements, page=0):
    if not api.is_database_empty():
        datamodels_uris = datamodels.split('+')
        languages_uris = languages.split('+')
        movements_names = movements.split('+')

        datamodels = api.get_datamodel_count()
        max_sames = api.get_max_nosql_count_sames()
        views_by_datamodels = api.get_sentiments_by_datamodel()

        homepage_filter = api.HomepageFilter()
        results = homepage_filter.filter(movements_names, datamodels_uris, languages_uris)

        total = len(results)
        context = {}
        page = request.GET.get('page', 1)
        paginator = Paginator(results, 9)

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        context['page'] = page
        context['results'] = results
        context['datamodels'] = datamodels
        context['total'] = total
        context['max_sames'] = max_sames
        context['views_by_datamodels'] = views_by_datamodels

        if not results:
            similar_languages = homepage_filter.filter_by_similar_languages(movements_names, datamodels_uris, languages_uris)
            datamodels_and_licenses = homepage_filter.filter_by_datamodels_and_licenses(movements_names, datamodels_uris)
            best_performance = homepage_filter.filter_by_best_performance()
            context['similar_languages'] = similar_languages
            context['datamodels_and_licenses'] = datamodels_and_licenses
            context['best_performance'] = best_performance

            return render(request, 'recommendations.html', context)

        return render(request, 'grid.html', context)
    else:
        return render(request, 'offline.html', {})


def nosql_detail(request, uri):
    nosql = NoSQL.nodes.get(uri=uri)
    context = {}

    context['nosql'] = nosql
    context['homepages'] = api.get_homepage(nosql.uri)
    context['latest_release'] = api.get_latest_release(nosql.uri)
    context['status'] = api.get_status(nosql.uri)
    context['frequently_updated'] = api.get_frequently_updated(nosql.uri)
    context['documentation'] = api.get_documentation(nosql.uri)

    site_stats = SiteStats()

    ## Data for the bullet charts

    bullet_stats = BulletStats()
    stackoverflow_bullet_data = bullet_stats.get_stackoverflow_views(nosql.uri)
    context['stackoverflow_bullet_data'] = stackoverflow_bullet_data

    github_bullet_data = bullet_stats.get_repository_stars(nosql.uri)
    context['github_bullet_data'] = github_bullet_data

    stackshare_bullet_data = bullet_stats.get_stackshare_votes(nosql.uri)
    context['stackshare_bullet_data'] = stackshare_bullet_data

    alternativeto_bullet_data = bullet_stats.get_alternativeto_likes(nosql.uri)
    context['alternativeto_bullet_data'] = alternativeto_bullet_data

    dbengine_bullet_data = bullet_stats.get_dbengine_score(uri)
    context['dbengine_bullet_data'] = dbengine_bullet_data
    
    github_ranking = site_stats.get_repositories_ranking(5)
    context['github_ranking'] = github_ranking

    stackshare_ranking = site_stats.get_stackshare_ranking(nosql.name)
    context['stackshare_ranking'] = stackshare_ranking

    alternativeto_ranking = site_stats.get_alternativeto_ranking(nosql.name)
    context['alternativeto_ranking'] = alternativeto_ranking

    github_position = site_stats.get_position_in_repositories_ranking(nosql.name)
    context['github_position'] = github_position

    stackshare_position = site_stats.get_position_in_stackshare_ranking(nosql.name)
    context['stackshare_position'] = stackshare_position

    alternativeto_position = site_stats.get_position_in_alternativeto_ranking(nosql.name)
    context['alternativeto_position'] = alternativeto_position

    context['datamodels'] = api.get_datamodels(nosql.uri)
    context['summary'] = api.get_summary(nosql.uri)
    context['developed_languages'] = api.get_developed_languages(nosql.uri)
    context['supported_datatypes'] = api.get_supported_datatypes(nosql.uri)
    context['nosql_rating'] = api.get_performance_rating(nosql.uri)

    stackoverflow_tag = api.get_stackoverflow_tag(nosql.uri)
    context['stackoverflow_tag'] = stackoverflow_tag

    context['stackoverflow_unanswered'] = api.get_percent_unanswered_questions(nosql.uri)

    alternativeto_post = api.get_alternativeto_post(nosql.uri)
    context['alternativeto_post'] = alternativeto_post

    stackshare_post = api.get_stackshare_post(nosql.uri)
    context['stackshare_post'] = stackshare_post
    context['stackshare_comments'] = api.get_stackshare_comments(nosql.uri)

    dbengine_post = api.get_dbengine_post(nosql.uri)
    context['dbengine_post'] = dbengine_post

    repository = api.get_repository(nosql.uri)
    context['repository'] = repository

    wikipedia_post = api.get_wikipedia_post(nosql.uri)
    context['wikipedia_post'] = wikipedia_post

    developers = api.get_developers(nosql.uri)
    context['developers'] = developers

    operating_systems = api.get_operating_systems(nosql.uri)
    context['operating_systems'] = operating_systems

    uses = api.get_uses(nosql.uri)
    context['uses'] = uses

    apis = api.get_apis(nosql.uri)
    context['apis'] = apis

    transactions = api.get_transaction_methods(nosql.uri)
    context['transactions'] = transactions

    consistencies = api.get_consistency_methods(nosql.uri)
    context['consistencies'] = consistencies

    repositories_by_language = api.get_repositories_by_language(nosql.uri)
    context['repositories_by_language'] = repositories_by_language

    technologies = api.get_supported_technologies(nosql.uri)
    context['technologies'] = technologies

    tools = api.get_supported_tools(nosql.uri)
    context['tools'] = tools

    context['drivers'] = api.get_drivers(nosql.uri)

    drivers_repos = api.get_drivers_repositories(nosql.uri)
    context['drivers_repos'] = drivers_repos

    clients = api.get_clients(nosql.uri)
    context['clients'] = clients

    links = api.get_links(nosql.uri)
    context['links'] = links

    alternativeto_recommendations = api.get_alternativeto_recommendations(nosql.uri)
    context['alternativeto_recommendations'] = alternativeto_recommendations

    context['associated_tags'] = api.get_associated_tags(nosql.uri)

    context['shared_tags'] = api.get_shared_tags(nosql.uri)

    interest_over_time = api.get_interest_over_time(nosql.uri)
    context['interest_over_time'] = interest_over_time
    context['is_valid_interest_over_time'] = any(item['queries'] > 0 for item in interest_over_time) and len(interest_over_time) > 0

    context['comments'] = api.get_comments(nosql.uri)
    context['comments_text'] = api.get_comments_text(nosql.uri)

    comments_percents = api.get_percents_comments(nosql.uri)
    context['comments_percents'] = comments_percents
    context['has_comments_percents'] = any(comments_percents[k] > 0 for k in comments_percents.keys()) and len(comments_percents) > 0

    context['comments_points'] = api.get_comments_points(nosql.uri)

    popular_apis = api.get_all_popular_apis()
    context['popular_apis'] = popular_apis

    popular_transactions = api.get_all_popular_transactions_methods()
    context['popular_transactions'] = popular_transactions

    popular_consistencies = api.get_all_popular_consistency_methods()
    context['popular_consistencies'] = popular_consistencies

    context['all_transactions'] = set(transactions + popular_transactions)
    context['all_apis'] = set(popular_apis + apis)
    context['all_consistencies'] = set(consistencies + popular_consistencies)

    all_stackoverflow_views = api.get_all_stackoverflow_views()
    context['all_stackoverflow_views'] = all_stackoverflow_views
    context['has_stackoverflow_views'] = any(item['name'] == nosql.name for item in all_stackoverflow_views)
    
    all_repositories_stars = api.get_all_repositories_stars()
    context['all_repositories_stars'] = all_repositories_stars
    context['has_repositories_stars'] = any(item['name'] == nosql.name for item in all_repositories_stars)

    all_stackshare_votes = api.get_all_stackshare_votes()
    context['all_stackshare_votes'] = all_stackshare_votes
    context['has_stackshare_votes'] = any(item['name'] == nosql.name for item in all_stackshare_votes)

    all_dbengine_scores = api.get_all_dbengine_scores()
    context['all_dbengine_scores'] = all_dbengine_scores
    context['has_dbengine_scores'] = any(item['name'] == nosql.name for item in all_dbengine_scores)

    all_alternativeto_likes = api.get_all_alternativeto_likes()
    context['all_alternativeto_likes'] = all_alternativeto_likes
    context['has_alternativeto_likes'] = any(item['name'] == nosql.name for item in all_alternativeto_likes)

    context['max_sames'] = api.get_max_nosql_count_sames()
    context['sames'] = api.get_count_sames(nosql.uri)

    return render(request, 'detail.html', context)


def get_similar_by_datamodel(request):
    uri = request.GET.get('uri')
    nosqls = api.get_similar_by_datamodel(uri)
    context = {}
    context['similars'] = nosqls
    html = render_to_string('comparisonTable.html', context)
    return HttpResponse(html)


def get_similar_by_stackoverflow_tags(request):
    uri = request.GET.get('uri')
    nosqls = api.get_similar_by_stackoverflow(uri)
    context = {}
    context['similars'] = nosqls
    html = render_to_string('comparisonTable.html', context)
    return HttpResponse(html)


def get_similar_by_performance(request):
    uri = request.GET.get('uri')
    nosqls = api.get_similar_by_performance(uri)
    context = {}
    context['similars'] = nosqls
    html = render_to_string('comparisonTable.html', context)
    return HttpResponse(html)


def get_similar_by_language(request):
    uri = request.GET.get('uri')
    languages = api.get_supported_languages_uris(uri)
    nosqls = api.get_similar_by_languages(uri, languages)
    context = {}
    context['similars'] = nosqls
    html = render_to_string('comparisonByLanguageTable.html', context)
    return HttpResponse(html)

def get_repositories_progressbars(request):
    uri = request.GET.get('uri')
    languages = api.find_language_stats(uri)
    context = {}
    context['uri'] = uri
    context['languages'] = languages
    html = render_to_string('repositoriesProgressbars.html', context)
    return HttpResponse(html)

def get_sentiment_pie(request):
    uri = request.GET.get('uri')
    sentiments = api.find_sentiments_percents(uri)
    context = {}
    context['uri'] = uri
    context['sentiments'] = sentiments
    html = render_to_string('sentimentsPie.html', context)
    return HttpResponse(html)

def get_interest_over_time(request):
    uri = request.GET.get('uri')
    interest = api.get_interest_over_time(uri)
    context = {}
    context['uri'] = uri
    context['interest'] = interest
    context['interest_difference'] = utils.percentage_difference(interest)

    html = render_to_string('interestOverTimeChart.html', context)
    return HttpResponse(html)

def get_popularity(request):
    uri = request.GET.get('uri')
    popularity = api.get_popularity(uri)
    popularity['uri'] = uri
    number_questions = popularity['number_questions']
    github_stars = popularity['github_stars']
    if popularity['percentage_unanswered']:
        popularity['percentage_answered'] = 100 - popularity['percentage_unanswered']
    elif number_questions > 0:
        popularity['percentage_answered'] = 100
        popularity['percentage_unanswered'] = 0
    else:
        popularity['percentage_answered'] = 0
        popularity['percentage_unanswered'] = 0
    popularity['number_questions'] = utils.number_format(number_questions)
    popularity['github_stars'] = utils.number_format(github_stars)
    popularity['comments'] = api.get_comments_points(uri)
    html = render_to_string('popularityChart.html', popularity)
    return HttpResponse(html)


def get_nosqls_by_datamodel(request, name):
    nosql_data = api.get_nosql_by_datamodel(name)
    page = request.GET.get('page', 1)
    paginator = Paginator(nosql_data, 9)

    try:
        nosql_data = paginator.page(page)
    except PageNotAnInteger:
        nosql_data = paginator.page(1)
    except EmptyPage:
        nosql_data = paginator.page(paginator.num_pages)
    datamodels_count = api.find_datamodel_count()

    site_stats = SiteStats()
    stack_ranking = site_stats.get_stackoverflow_ranking(5)
    github_ranking = site_stats.get_repositories_ranking(5)

    context = {'page': page, 'nosql_data': nosql_data,
               'datamodels_count': datamodels_count,
               'stack_ranking': stack_ranking,
               'github_ranking': github_ranking}
    return render(request, 'grid.html', context)


class InterestOverTime(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        uri = request.query_params.get("uri", None)
        interest = []
        if uri is not None:
            interest = api.get_interest_over_time(uri)
        return Response(interest)


class RepositoriesByLanguage(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        uri = request.query_params.get("uri", None)
        languages = []
        if uri is not None:
            languages = api.find_language_stats(uri)
        return Response(languages)


class SentimentChart(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        uri = request.query_params.get("uri", None)
        sentiments = {}
        if uri is not None:
            sentiments = api.find_sentiments_percents(uri)
        return Response(sentiments)


class RepositoriesByLanguageData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        nosql = request.query_params.get("nosql", None)
        lang = request.query_params.get("lang", None)
        repos = []
        if nosql is not None and lang is not None:
            repos = api.get_repos_by_language(nosql, lang)
        return Response(repos)


class InterestOverTime(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        nosql = request.query_params("uri", None)
        interest = {}
        if uri is not None:
            interest = api.get_interest_over_time(uri)
        return Response(interest)
