$(document).ready(function() {
    max = $("#sites-bullet-chart").data('max');
    measure = $("#sites-bullet-chart").data('measure');
    
    AmCharts.makeChart("sites-bullet-chart", {
        "theme": "light",
        "type": "serial",
        "hideCredits": true,
        "rotate": true,
        "dataProvider": [{
            "site": "Stack Overflow",
            "max": max,
            "actual": measure,
            "colorAverage": "#1cb4bb",
            "colorMax": "#05d2dc",
            "colorActual": "#33a9af"
        }],
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0,
            "maximum": 7,
            "minimum": 0,
            "autoGridCount": false,
            "gridCount": 7,
            "labelsEnabled": false
        }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "Max: <b>[[value]]</b>",
            "fillAlphas": 0.5,
            "lineAlpha": 0,
            "bullet": "round",
            "type": "column",
            "clustered":false,
            "valueField": "max",
            "colorField": "colorMax"
        },
         {
            "balloonText": "Actual: <b>[[value]]</b>",
            "fillAlphas": 0.7,
            "lineAlpha": 0.2,
            "bullet": "round",
            "type": "column",
            "clustered":false,
            "valueField": "actual",
            "colorField": "colorActual"
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "site",
        "categoryAxis": {
            "gridPosition": "start",
            "labelsEnabled": false
        },
        "export": {
            "enabled": false
         }
    });
});