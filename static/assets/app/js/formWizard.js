$(document).ready(function() {
	var ctx1 = document.getElementById("background-chart").getContext("2d");

	// Create Linear Gradient
	var white_gradient = ctx1.createLinearGradient(0, 0, 0,500);
	white_gradient.addColorStop(0, 'rgba(70,128,255,0.5)');
	white_gradient.addColorStop(1, 'rgba(70,128,255,0)');

	// Chart Options
	var backgroundOptions = {
	    responsive: true,
	    maintainAspectRatio: false,
	    datasetStrokeWidth : 3,
	    pointDotStrokeWidth : 4,
	    tooltipFillColor: "rgba(0,0,0,0.8)",
	    responsive: true,
	    tooltips: {
	     enabled: false
		},	
	    legend: {
	        display: false,
	    },
	    hover: {
	        mode: 'label'
	    },
	    scales: {
	        xAxes: [{
	            display: false,
	        }],
	        yAxes: [{
	            display: false,
	            ticks: {
	                min: 0,
	                max: 85
	            },
	        }]
	    },
	    title: {
	        display: false,
	        fontColor: "#FFF",
	        fullWidth: false,
	        fontSize: 40,
	        text: '82%'
	    }
	};

	// Chart Data
	var backgroundData = {
	    labels: ["January", "February", "March", "April", "May", "June", "July"],
	    datasets: [{
	        label: "Employees",
	        data: [28, 35, 36, 48, 46, 42, 60],
	        backgroundColor: white_gradient,
	        borderColor: "rgba(255,255,255,1)",
	        borderWidth: 2,
	        strokeColor : "#ff6c23",
	        pointColor : "#fff",
	        pointBorderColor: "rgba(255,255,255,1)",
	        pointBackgroundColor: "#3BAFDA",
	        pointBorderWidth: 2,
	        pointHoverBorderWidth: 2,
	        pointRadius: 5,
	    }]
	};

    var backGroudconfig = {
        type: 'line',

        // Chart Options
        options : backgroundOptions,

        // Chart Data
        data : backgroundData
    };

    // Create the chart
    var areaChart = new Chart(ctx1, backGroudconfig);
});